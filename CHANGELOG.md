# 1.0.0.beta.14

  - Updated test case title template function to replace `$` arguments.

    - works for objects, classes and pure function tests
    - `$` arguments are 1 based. i.e. `$1` will be replaced by the first case argument.
    - `$i` will be replaced by the case index which is also 1 based.

    **note**: `(case #)` is no longer automatically appended to the title.

    Example
    ```js
    'expected $1 to be $2 (case $i)': [
      [[5, 10]],
      function (actual, expected) {
        assert.equal(this.test.title, "expected 5 to be 10 (case 1)");
      }
    ],
    ```

    The template is still customizable using `options.testCaseTitleTemplate`

    ```ts
    // now includes a test case array argument
    (title: string, caseArgs: Array<any[]>, caseIndex: number) => string
    ```

# 1.0.0.beta.13

  - Added sort option MochaUiEsmOptions

    Can be either:
    - `"all"`
    - `"groupsOnly"`
    - `"testsOnly"`; or
    - `"none"` (default)

# 1.0.0.beta.12

- Updated esm-test-parser@1.0.0-beta.11
  - **object tests** Adds the ability to have single test cases without writing array brackets
  ```js
  const fooTests = {
    test1: [
      1,
      2,
      (singleTestCaseArg) => {
        assert.ok(
          singleTestCaseArg == 1
            || singleTestCaseArg == 2
        )
      }
    ]
  }
  ```
- Added parserFlags to MochUiEsmOptions for ability to disable
  parsing literal fields (for disambiguation scenarios)
  - `parserFlags.allowLiteralOnly` (default is true)
  - `parserFlags.allowLiteralTitle` (default is true)
  - `parserFlags.allowLiteralCases` (default is true)

# 1.0.0.beta.11

- Breaking change: @only is now @testOnly
  ```js
  @testOnly()
  export class FooTests {}
  ```

- Added ability to change the test case title template
  ```js
  const options = {
    testCaseTitleTemplate: (title, caseIndex) => 
      `${title} (case ${caseIndex})` // the current default
  }
  registerMochaUiEsm(options)
  ```

- Added exports for 
  - `only` 
  - `test.cases`
  - `test.only` 
  - `test.title`
  
  These can be used as identifiers imported via intellisense

    Object tests
    ```js
      import {only, test} from 'mocha-ui-esm'
      export const FooTests = {
        [only]: 1, // or [test.only]: 1
        [test.title]: 'test title',

        // literals still work
        title: 'still works'
        only: 1
      }
    ```

    Function tests
    ```js
      import {only, test} from 'mocha-ui-esm'
      fooTest1[only] = true; // or fooTest1[test.only] = true
      fooTest1[test.title] = true;
      export function fooTest1() { }

      fooTest2[test.cases] = [ // or fooTest2["testCases"]
        [Math.PI, 3],
        [Math.PI * 2, 6]
      ]
      export function fooTest2(testRadians, expectedFloor) {
        const actual = Math.floor(testRadians);
        assert.equal(actual, expectedFloor);
      }

# 1.0.0.beta.10

- Updated `esm-test-parser@1.0.0-beta.9` 
  - Added ability to provide a title to modules (not for root suite yet)

    See [Module Title Tests](./test/e2e/modules/title.tests.js)

    Also see [Export Module Title Groups](./test/e2e/classes/all.tests.js)
    ```js
    // this will override the parent export "as" keyword name
    export const title = "My module test group name"
    ```

# 1.0.0.beta.9

- Updated `esm-test-parser@1.0.0-beta.8` 
  - Added ability to provide a title to object test groups
    ```js
    export const myTests = {
      title: "test title"
    }
    ```

# 1.0.0.beta.8

- Updated `esm-test-parser@1.0.0-beta.7` 
  - Added pure function testing feature for quick simple tests and proto typing
  - Fixes an `only` combined with `test cases` bug

# 1.0.0.beta.7

- Updated `esm-test-parser@1.0.0-beta.6` 
  - Fixes an only:'*' grouping bug where sibiling groups were not marked to run as only

# 1.0.0-beta.6

- Updated `esm-test-parser` so this plugin supports only expressions. '*', '#num'. 
  
  Examples
  - `only: '*'` will run all the following tests. Decorator syntax `@only("*")`
  - `only: '2'` will run the next two tests. Decorator syntax `@only(2)`
  - `only: true` still runs the one test. Decorator syntax `@only()`

# 1.0.0-beta.5

- Replaced internal extractTestsFromModule function with 'esm-parser-test' for parsing tests
  - This fixes a condition where @only() class test function was not running as 'only' test

# 1.0.0-beta.4

- Fixed class testCase execution order
- Extracted parser logic in to esm-test-parser
- Transfered mocha-ui-esm to new repo https://gitlab.com/esm-test-group/mocha-ui-esm

# 1.0.0-beta.3

- Added babel decorator support
- Changed case title number to start from 1. So the first test case title will appear as `test1 (case 1)`
  `caseIndex` passed to each test case function still starts from 0


# 1.0.0-beta.2

- Added `@only()` decorator support for class group tests and class function tests.
- Added `@testCase()` decorator support for class function case testing.
- Added `@testTitle()` decorator support for naming tests with a custom title.
- Added option to set the property name used for accessing the context inside class tests. 
  The default value is `options.classContextPropertyName='suiteContext'`
- Added types configuration for typescript `moduleResolution=Node16|NodeNext` support.

# 1.0.0-beta.1

- Added support to run non-instatiated class tests
- Updated this project to be an ES module type.
  - **Note**: It also compiles and publishes *.cjs for legacy support
- This module exports a function called registerMochaUiEsm (also exported as the default). 
  This function must be called to register itself with mocha.

  From an ES module project
  ```js
  import registerMochaUiEsm from 'mocha-ui-esm'
  registerMochaUiEsm()
  ```

  From a CJS project
  ```js
  const { registerMochaUiEsm } = require('mocha-ui-esm'); 
  // or 
  require('mocha-ui-esm').default()
  ```

# 1.0.0-alpha.7

- Removed 'default' text entries from test results whilst still preserving their child test suites. 
  This only occurs when using anonymous `export default` statements

# 1.0.0-alpha.6

- added ability for multiple test cases per test function (not implemented for class testing yet)
  See [Case Test Examples](/../master/test/unit/cases/case.tests.js)
