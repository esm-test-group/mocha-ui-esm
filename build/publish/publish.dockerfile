# see ./publish.gitlab-ci.yml for cmdline example of how to run on local docker
FROM node:18.12.1-buster-slim
ARG TARGET_PATH=/mocha-ui-esm

COPY / $TARGET_PATH

WORKDIR $TARGET_PATH
RUN npm install
CMD npm publish