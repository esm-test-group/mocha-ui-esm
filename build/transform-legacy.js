import fs from 'node:fs';
import path from 'node:path';

const projectPath = process.cwd();
const legacyPath = path.resolve(projectPath, "legacy");
const jsFiles = getFiles(legacyPath)

jsFiles.filter(file => path.extname(file) == '.js')
  .forEach(file => {
    const filename = path.parse(file).name;

    // replace all '.js' with '.cjs'
    const jsFile = path.resolve(legacyPath, `${filename}.js`);
    const jsContents = fs.readFileSync(jsFile, 'utf8');
    fs.writeFileSync(jsFile, jsContents.replaceAll('.js', '.cjs'));

    // replace the first entry of '.js' with '.cjs'
    const mapFile = path.resolve(legacyPath, `${filename}.js.map`);
    const mapContents = fs.readFileSync(mapFile, 'utf8');
    fs.writeFileSync(mapFile, mapContents.replace('.js', '.cjs'));

    // rename *.js to *.cjs
    fs.renameSync(
      jsFile,
      path.resolve(legacyPath, `${filename}.cjs`)
    );

    // rename *.js.map to *.cjs.map
    fs.renameSync(
      mapFile,
      path.resolve(legacyPath, `${filename}.cjs.map`)
    );
  });

function getFiles(absolutePath) {
  return fs.readdirSync(absolutePath)
    .filter(entry => fs.statSync(path.resolve(absolutePath, entry)).isFile())
}