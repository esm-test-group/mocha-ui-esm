import assert from 'assert'
import { test } from 'mocha-ui-esm'

export const ExampleTestObject = {

  [test.title]: "Example test object",

  // 'only': true // to run single tests
  'test 1': () => {
    assert.ok(true)
  },

  'test 1': function () {
    assert.ok(true)
  },

  'test with multiple test cases': [
    [1.123, 1],
    [2.567, 2],
    function (test_arg, expected) {
      assert.equal(Math.floor(test_arg), expected);
    }
  ]

}