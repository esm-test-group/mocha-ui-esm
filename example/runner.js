import Mocha from 'mocha'
import SourceMaps from 'source-map-support'
import registerMochaUiEsm from '../src/index.js'

import * as Tests from './tests.js'

// register esm test intergration
registerMochaUiEsm();

// register soucemap support
SourceMaps.install()

// create the test runner
const runner = new Mocha({
  ui: 'esm',
  reporter: typeof window != 'undefined' ? 'html' : 'spec',
  color: true
})

// register the esm tests
runner.suite.emit('modules', Tests)

// execute the tests
runner.run(
  failures => {
    if (process) process.exit(failures)
  }
)