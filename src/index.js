import * as Mocha from 'mocha';
import { MochaUiEsmOptions } from './mochaUiEsmOptions.js';
import { getTestsFromEsmModule } from './getTestsFromEsmModule.js';

/**
 * @param {MochaUiEsmOptions} options 
 */
export function registerMochaUiEsm(options) {
  Mocha.interfaces.esm = function (suite) {
    suite.on(
      'modules',
      modules => getTestsFromEsmModule(
        suite,
        new MochaUiEsmOptions(options),
        modules
      )
    );
  }
}

export default registerMochaUiEsm;
export * from 'esm-test-parser';