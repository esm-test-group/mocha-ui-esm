import { SortByEnum, TestParserFlags } from "esm-test-parser";

export const builtInFunctions = [
  'beforeAll',
  'beforeEach',
  'afterAll',
  'afterEach'
];

/**
 * @type {MochaUiEsmOptions} defaultOptions
 */
const defaultOptions = {
  classContextPropertyName: "suiteContext",
  testParserFlags: {
    allowLiteralOnly: true,
    allowLiteralTitle: true,
    allowLiteralCases: true
  },
  sort: SortByEnum.none
}

export class MochaUiEsmOptions {

  /**
   * @param {MochaUiEsmOptions|Object} options 
   */
  constructor (options) {
    Object.assign(this, defaultOptions, options);
  }

  /**
   * Sets the property name used for accessing the context inside class tests
   * The default value is 'suiteContext'
   *
   * ```ts
   *  test1() {
   *    console.log(this.suiteContext.test.title)
   *  }
   * ```
   *  @type {string | undefined}
   */
  classContextPropertyName;

  /**
   * Test case title template function.
   * 
   * The default function will replace $num args with test case argument values
   * 
   * Example
   * - test case args [[5, 10]]
   * - title input `expected $1 to be $2 (case $i)`
   * - title output `expected 5 to be 10 (case 1)`
   * @type {(title: string, testCases: Array<any[]>, caseIndex: number) => void}
   */
  testCaseTitleTemplate;

  /**
   * @type {TestParserFlags | undefined}
   */
  parserFlags;

    /**
     * Can be either
     * - "none" (default)
     * - "byGroup"
     * - "byTitle"
     * - "byTitleAndGroup"
     * @type {SortByEnum | undefined}
     */
  sort;

}