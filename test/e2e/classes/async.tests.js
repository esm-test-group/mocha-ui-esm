import assert from 'assert'
import { testTitle } from 'mocha-ui-esm';
import { OneMs, TwoMs } from '../utils.js'

@testTitle("Async")
export class ClassAsyncTests {

  beforeAll() {
    this.asyncCalls = 0;
  }

  afterAll() {
    assert.ok(
      this.asyncCalls === 2,
      `AfterAll was not called. ${this.asyncCalls}`
    );
  }

  async await() {
    await OneMs()
    this.asyncCalls++
  }

  promise(done) {
    TwoMs()
      .then(_ => this.asyncCalls++)
      .then(_ => done())
      .catch(err => done(err))
  }

}