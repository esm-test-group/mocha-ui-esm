import assert from 'assert'
import { testTitle } from 'mocha-ui-esm'

@testTitle("Basic")
export class ClassBasicTests {

  constructor () {
    this.beforeEachCalled = 0
  }

  beforeEach() {
    this.beforeEachCalled++
  }

  test1() {
    assert.ok(
      this.beforeEachCalled === 1,
      `beforeEach was not called. ${this.beforeEachCalled}`
    )
  }

  test2() {
    assert.ok(
      this.beforeEachCalled === 2,
      `beforeEach was not called. ${this.beforeEachCalled}`
    )
  }

}