import assert from 'assert'
import { testCase, testTitle } from 'mocha-ui-esm';
import { OneMs, TwoMs } from '../utils.js';

@testTitle("TestCases")
export class ClassTestCaseTests {

  constructor () {
    this.calledCases = 0
  }

  @testCase(1, 2, 3)
  singleClassTestCase(arg1, arg2, arg3, caseIndex) {
    assert.equal(arg1, 1);
    assert.equal(arg2, 2);
    assert.equal(arg3, 3);
    assert.equal(this.suiteContext.test.title, "singleClassTestCase");
  }

  @testCase(Math.PI, 3)
  @testCase(123.24235, 123)
  multipleClassTestCases(test, expected, caseIndex) {
    assert.equal(Math.floor(test), expected);
    assert.equal(this.suiteContext.test.title, "multipleClassTestCases");
  }

  @testTitle("caseIndexClassTestCases (case $i)")
  @testCase()
  @testCase()
  @testCase()
  caseIndexClassTestCases(caseIndex) {
    assert.equal(caseIndex, this.calledCases);
    assert.equal(
      this.suiteContext.test.title,
      `caseIndexClassTestCases (case ${caseIndex + 1})`
    );

    this.calledCases++
  }

  @testCase(Math.PI, 3)
  @testCase(123.24235, 123)
  async asyncClassTestCases(test, expected, caseIndex) {
    await OneMs()
    assert.equal(Math.floor(test), expected);
  }

  @testCase(Math.PI, 3)
  @testCase(123.24235, 123)
  promiseClassTestCases(test, expected, caseIndex, done) {
    assert.ok(done instanceof Function);

    Promise.resolve(TwoMs)
      .then(() => {
        assert.equal(Math.floor(test), expected);
        done()
      })
      .catch(err => done(err));
  }

}