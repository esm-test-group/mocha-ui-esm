import assert from 'assert'
import { testTitle } from 'mocha-ui-esm'

class AbstractTests {

  constructor() {
    this.beforeEachCalled = 0
  }

  beforeEach() {
    this.beforeEachCalled++
  }

  test1() {
    throw new Error("Not implemented")
  }

  test3() {
    assert.ok(
      this.beforeEachCalled === 3,
      `beforeEach was not called. ${this.beforeEachCalled}`
    )
  }
}

@testTitle("Extends")
export class ClassExtendsTests extends AbstractTests {

  constructor() {
    super()
  }

  test1() {
    assert.ok(
      this.beforeEachCalled === 1,
      `beforeEach was not called. ${this.beforeEachCalled}`
    )
  }

  test2() {
    assert.ok(
      this.beforeEachCalled === 2,
      `beforeEach was not called. ${this.beforeEachCalled}`
    )
  }

}
