import assert from "assert";

let testContext = null;

export function beforeAll() {
  testContext = {
    testsCalled: 0,
    eachCalled: 0
  }
}

export function afterAll() {
  assert.equal(testContext.testsCalled, 2);
  assert.equal(testContext.eachCalled, testContext.testsCalled * 2);
}

export function beforeEach() {
  testContext.eachCalled++;
}

export function afterEach() {
  testContext.eachCalled++;
}

export function basicFuncTest1() {
  assert.ok(true);
  testContext.testsCalled++;
}

export function basicFuncTest2() {
  assert.ok(true);
  testContext.testsCalled++;
}
