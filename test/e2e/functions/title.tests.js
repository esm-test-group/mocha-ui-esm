import assert from 'assert'
import { test } from 'mocha-ui-esm'

testFunctionTitle1[test.title] = "Function test title 1";
export function testFunctionTitle1() {
  assert.equal(this.test.title, "Function test title 1")
}

testFunctionTitle2[test.title] = "Function test title 2";
export function testFunctionTitle2() {
  assert.equal(this.test.title, "Function test title 2")
}