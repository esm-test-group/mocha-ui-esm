import assert from 'assert'

// exporting a "title" sets the module test title
// will override parent export "as" keyword name
export const title = "ModuleBasicTitleTests -> test module title";

const expectedObjectTitle = "ModuleObjectTitleTests";

export const ModuleObjectTitleTests = {

  beforeEach: function () {
    assert.equal(this.test.parent.parent.title, title)
    assert.equal(this.test.parent.title, expectedObjectTitle)
  },

  test1: function () {
    assert.equal(this.test.parent.parent.title, title)
    assert.equal(this.test.parent.title, expectedObjectTitle)
  },

  test2: function () {
    assert.equal(this.test.parent.parent.title, title)
    assert.equal(this.test.parent.title, expectedObjectTitle)
  },

}

const expectedClassTitle = "ModuleClassTitleTests";

export class ModuleClassTitleTests {

  beforeEach() {
    assert.equal(this.suiteContext.test.parent.parent.title, title)
    assert.equal(this.suiteContext.test.parent.title, expectedClassTitle)
  }

  test1() {
    assert.equal(this.suiteContext.test.parent.parent.title, title)
    assert.equal(this.suiteContext.test.parent.title, expectedClassTitle)
  }

  test2() {
    assert.equal(this.suiteContext.test.parent.parent.title, title)
    assert.equal(this.suiteContext.test.parent.title, expectedClassTitle)
  }

}