import assert from 'assert'
import { test } from 'mocha-ui-esm'
import { OneMs, TwoMs } from '../utils.js'

const testContext = {
  asyncCalls: 0
}

export const ObjectAsyncTests = {

  [test.title]: "Async",

  beforeAll: () => {
    testContext.asyncCalls = 0
  },

  afterAll: () => {
    assert.ok(testContext.asyncCalls === 2, `AfterAll was not called. ${testContext.asyncCalls}`)
  },

  'await': async () => {
    await OneMs()
    testContext.asyncCalls++
  },

  'promise': done => {
    TwoMs()
      .then(_ => testContext.asyncCalls++)
      .then(_ => done())
      .catch(err => done(err))
  }

}