import assert from 'assert'
import { test } from 'mocha-ui-esm'

const testContext = {
  afterAllCalled: 0
}

export const ObjectAfterAllTests = {

  [test.title]: "afterAll",

  afterAll: () => {
    testContext.afterAllCalled++
    assert.ok(testContext.afterAllCalled === 3, `AfterAll was not called. ${testContext.afterAllCalled}`)
  },

  'ObjectAfterAllTests -> group 1 -> test 1': () => { },
  'ObjectAfterAllTests -> group 1 -> test 2': () => { },

  'ObjectAfterAllTests -> group 2': {

    afterAll: () => {
      testContext.afterAllCalled++
      assert.ok(testContext.afterAllCalled === 2, `AfterAll was not called. ${testContext.afterAllCalled}`)
    },

    'ObjectAfterAllTests -> group 2 -> test 1': () => { },
    'ObjectAfterAllTests -> group 2 -> test 2': () => { },

    'ObjectAfterAllTests -> group 3': {

      afterAll: () => {
        testContext.afterAllCalled = 1
        assert.ok(testContext.afterAllCalled === 1, `AfterAll was not called. ${testContext.afterAllCalled}`)
      },

      'ObjectAfterAllTests -> group 3 -> test 1': () => { },
      'ObjectAfterAllTests -> group 3 -> test 2': () => { },
    }

  }

}