import assert from 'assert'
import { test } from 'mocha-ui-esm'

const testContext = {
  afterEachCalled: 0
}

export const ObjectAfterEachTests = {

  [test.title]: "afterEach",

  beforeAll: () => {
    testContext.afterEachCalled = 0
  },

  afterEach: () => {
    testContext.afterEachCalled++
  },

  afterAll: () => {
    assert.equal(testContext.afterEachCalled, 12)
  },

  'ObjectAfterEachTests -> group 1 -> test 1': () => {
    assert.ok(testContext.afterEachCalled === 0, `afterEach was not called ${testContext.afterEachCalled}`)
  },

  'ObjectAfterEachTests -> group 1 -> test 2': () => {
    assert.ok(testContext.afterEachCalled  === 1, `afterEach was not called ${testContext.afterEachCalled}`)
  },

  'ObjectAfterEachTests -> group 2': {

    afterEach: () => {
      testContext.afterEachCalled++
    },

    'ObjectAfterEachTests -> group 2 -> test 1': () => {
      assert.ok(testContext.afterEachCalled  === 2, `afterEach was not called ${testContext.afterEachCalled}`)
    },

    'ObjectAfterEachTests -> group 2 -> test 2': () => {
      assert.ok(testContext.afterEachCalled  === 4, `afterEach was not called ${testContext.afterEachCalled}`)
    },

    'ObjectAfterEachTests -> group 3': {

      afterEach: () => {
        testContext.afterEachCalled++
      },

      'ObjectAfterEachTests -> group 3 -> test 1': () => {
        assert.ok(testContext.afterEachCalled  === 6, `afterEach was not called ${testContext.afterEachCalled}`)
      },

      'ObjectAfterEachTests -> group 3 -> test 2': () => {
        assert.ok(testContext.afterEachCalled  === 9, `afterEach was not called ${testContext.afterEachCalled}`)
      }

    }

  }

}