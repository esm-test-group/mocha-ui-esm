import assert from 'assert'
import { test } from 'mocha-ui-esm'

const testContext = {
  beforeEachCalled: 0
}

export const ObjectBeforeEachTests = {

  [test.title]: "beforeEach",

  beforeAll: () => {
    testContext.beforeEachCalled = 0
  },

  afterAll: () => {
    assert.equal(testContext.beforeEachCalled, 12)
  },

  beforeEach: () => {
    testContext.beforeEachCalled++
  },

  'ObjectBeforeEachTests -> group 1 -> test 1': () => {
    assert.ok(testContext.beforeEachCalled === 1, `beforeEach was not called ${testContext.beforeEachCalled}`)
  },

  'ObjectBeforeEachTests -> group 1 -> test 2': () => {
    assert.ok(testContext.beforeEachCalled === 2, `beforeEach was not called ${testContext.beforeEachCalled}`)
  },

  'ObjectBeforeEachTests -> group 2': {

    beforeEach: () => {
      testContext.beforeEachCalled++
    },

    'ObjectBeforeEachTests -> group 2 -> test 1': () => {
      assert.ok(testContext.beforeEachCalled === 4, `beforeEach was not called ${testContext.beforeEachCalled}`)
    },

    'ObjectBeforeEachTests -> group 2 -> test 2': () => {
      assert.ok(testContext.beforeEachCalled === 6, `beforeEach was not called ${testContext.beforeEachCalled}`)
    },

    'ObjectBeforeEachTests -> group 3': {

      beforeEach: () => {
        testContext.beforeEachCalled++
      },

      'ObjectBeforeEachTests -> group 3 -> test 1': () => {
        assert.ok(testContext.beforeEachCalled === 9, `beforeEach was not called ${testContext.beforeEachCalled}`)
      },

      'ObjectBeforeEachTests -> group 3 -> test 2': () => {
        assert.ok(testContext.beforeEachCalled === 12, `beforeEach was not called ${testContext.beforeEachCalled}`)
      }

    }

  }

}