import assert from 'assert'
import { test } from 'mocha-ui-esm'

const testContext = {
  indexCasesExecuted: 0
}

export const ObjectCaseTests = {

  [test.title]: "TestCases",

  beforeAll: () => {
    testContext.indexCasesExecuted = 0
  },

  afterAll: () => {
    assert.equal(testContext.indexCasesExecuted, 2)
  },

  'single case array': [
    [1, 2, 3],
    function (arg1, arg2, arg3) {
      assert.equal(arg1, 1)
      assert.equal(arg2, 2)
      assert.equal(arg3, 3)
    }
  ],

  'mulitple cases': [
    [4, 5, 6, 15],
    [7, 8, 9, 24],
    function (arg1, arg2, arg3, expected) {
      assert.equal(arg1 + arg2 + arg3, expected)
    }
  ],

  'mulitple cases with replace - $1 $2 $3 $4 (case $i)': [
    [4, 5, 6, 15],
    [7, 8, 9, 24],
    function (arg1, arg2, arg3, expected, caseIndex) {
      const expectedTitle = "mulitple cases with replace - "
        + `${arg1} ${arg2} ${arg3} ${expected} (case ${caseIndex + 1})`;

      assert.equal(arg1 + arg2 + arg3, expected)
      assert.equal(this.test.title, expectedTitle)
    }
  ],

  'single argument cases': [
    0,
    1,
    2,
    function (testSingleCase, caseIndex) {
      assert.equal(testSingleCase, caseIndex);
    }
  ],

  'case indexing (case $i)': [
    [4, 5, 6],
    [7, 8, 9],
    function (arg1, arg2, arg3, caseIndex) {
      testContext.indexCasesExecuted++;
      if (caseIndex == 0) {
        assert.equal(arg1, 4)
        assert.equal(arg2, 5)
        assert.equal(arg3, 6)
      } else if (caseIndex == 1) {
        assert.equal(arg1, 7)
        assert.equal(arg2, 8)
        assert.equal(arg3, 9)
      } else {
        throw new Error("Case index is invalid")
      }

      // test the mocha suite context is set
      assert.equal(this.test.title, `case indexing (case ${caseIndex + 1})`)
    }
  ]
}