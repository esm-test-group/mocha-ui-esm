import assert from 'assert'

export default {
  'no default entries': function () {
    assert.ok(this.test.parent.title != "default")
  }
}