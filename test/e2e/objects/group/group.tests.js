import { test } from 'mocha-ui-esm'
import a from './partial1.js'
import b from './partial2.js'

export const ObjectGroupTests = {
  [test.title]: "Grouped",

  mygroup: { ...a, ...b }
}