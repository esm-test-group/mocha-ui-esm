import assert from 'assert'
import { test } from 'mocha-ui-esm';

const expectedTitle = "Titles";

export const ObjectBasicTitleTests = {

  [test.title]: expectedTitle,

  beforeEach: function () {
    assert.equal(this.test.parent.title, expectedTitle)
  },

  test1: function () {
    assert.equal(this.test.parent.title, expectedTitle)
  },

  test2: function () {
    assert.equal(this.test.parent.title, expectedTitle)
  },

}

export const ObjectGroupTitleTests = {

  [test.title]: "ObjectGroupTitleTests -> group 1",

  beforeEach: function () {
    assert.equal(this.test.parent.title, "ObjectGroupTitleTests -> group 1")
  },

  'ObjectGroupTitleTests -> group 1 -> test 1': () => { },

  "ObjectGroupTitleTests -> group 2": {

    beforeEach: function () {
      assert.equal(this.test.parent.title, "ObjectGroupTitleTests -> group 2")
    },

    'ObjectGroupTitleTests -> group 2 -> test 1': () => { },
  }

}