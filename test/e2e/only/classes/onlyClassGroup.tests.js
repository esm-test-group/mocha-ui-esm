import assert from 'assert'
import { testOnly } from 'mocha-ui-esm';

@testOnly()
export class OnlyClassGroupTests {

  constructor () {
    this.testsRan = 0;
  }

  afterAll() {
    assert.equal(
      this.testsRan,
      2,
      "Only decorator on 'OnlyClassGroupTests' class failed to run all test functions"
    )
  }

  onlyClassGroupTest1() {
    assert.ok(true);
    this.testsRan++;
  }

  onlyClassGroupTest2() {
    assert.ok(true);
    this.testsRan++;
  }

}

export class ShouldNotRunClassTests {

  constructor () { }

  shouldNotRun() {
    assert.ok(false, "Only decorator on 'OnlyClassGroupTests' class has failed")
  }

}