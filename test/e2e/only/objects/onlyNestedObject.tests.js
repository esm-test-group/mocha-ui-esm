import assert from 'assert'

const testContext = {}

export const OnlyNestedObjectTests = {
  beforeAll: () => {
    testContext.beforeEachCalled = 0
  },
 
  beforeEach: () => {
    testContext.beforeEachCalled++
  },

  only: true,

  'only nested level 1': {
    beforeEach: () => {
      testContext.beforeEachCalled++
    },

    'incremental only nest test 1.1': () => {
      assert.ok(testContext.beforeEachCalled === 2, `beforeEach was not called ${testContext.beforeEachCalled}`)
    },

    'incremental only nest test 1.2': () => {
      assert.ok(testContext.beforeEachCalled === 4, `beforeEach was not called ${testContext.beforeEachCalled}`)
    },

  },

  'called': () => {
    assert.ok(false, "only not honoured")
  }

}