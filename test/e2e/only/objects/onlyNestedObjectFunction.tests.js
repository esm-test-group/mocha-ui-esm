import assert from 'assert'

const testContext = {}

export const onlyNestedObjectFunctionTests = {
  beforeAll: () => {
    testContext.beforeEachCalled = 0
  },

  beforeEach: () => {
    testContext.beforeEachCalled++
  },

  'nested level 1': {

    beforeEach: () => {
      testContext.beforeEachCalled++
    },

    only: true,
    'incremental only true nest test 1.1': () => {
      assert.ok(testContext.beforeEachCalled  === 2, `beforeEach was not called ${testContext.beforeEachCalled}`)
    },

    'called again': () => {
      assert.ok(false, "only not honoured")
    },

    'nested level 2': {

      beforeEach: () => {
        testContext.beforeEachCalled++
      },

      'called': () => {
        assert.ok(false, "only not honoured")
      }

    }

  },

  'called': () => {
    assert.ok(false, "only not honoured")
  }

}