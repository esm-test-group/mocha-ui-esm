import assert from 'assert'

const testContext = {}

export const onlyStarNestedObjectTests = {
  beforeAll: () => {
    testContext.beforeEachCalled = 0
  },

  'nested level 1': {
    only: '*',
    beforeEach: () => {
      testContext.beforeEachCalled++
    },

    'incremental only * nest test 1.1': () => {
      assert.ok(testContext.beforeEachCalled === 1, `beforeEach was not called ${testContext.beforeEachCalled}`)
    },

    'incremental only * nest test 1.2': () => {
      assert.ok(testContext.beforeEachCalled === 2, `beforeEach was not called ${testContext.beforeEachCalled}`)
    },

  },

  'called': () => {
    assert.ok(false, "only not honoured")
  }

}