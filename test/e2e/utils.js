export function OneMs() {
  return new Promise(function (resolve) {
    setTimeout(resolve, 1)
  })
}

export function TwoMs() {
  return new Promise(function (resolve) {
    setTimeout(resolve, 2)
  })
}