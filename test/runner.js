import Mocha from 'mocha'
import SourceMaps from 'source-map-support'
import { registerMochaUiEsm, SortByEnum } from '../src/index.js'

import * as TestOnlyModules from './e2e/only/all.only.tests.js'
import * as TestModules from './e2e/tests.js'

// register esm test intergration
registerMochaUiEsm({
  sort: SortByEnum.groupsOnly
});

// register soucemap support
SourceMaps.install()

// create the test runner
const runner = new Mocha({
  ui: 'esm',
  reporter: typeof window != 'undefined' ? 'html' : 'spec',
  color: true,
  timeout: 10000
})

// register the esm tests
if (process.env.TEST_ONLY) {
  runner.suite.emit('modules', TestOnlyModules)
} else {
  runner.suite.emit('modules', TestModules)
}

// execute the tests
runner.run(
  failures => {
    if (process) process.exit(failures)
  }
)